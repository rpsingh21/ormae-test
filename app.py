from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{app.root_path}/posts.db"
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def __repr__(self):
        return "<User %r>" % self.username


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    body = db.Column(db.String(1000), nullable=False)
    user_id = db.Column(db.Integer, nullable=False)


@app.route("/posts", methods=["GET"])
def get_posts():
    user_id = request.args.get("userId")

    if user_id:
        posts = Post.query.filter_by(user_id=user_id)
    else:
        posts = Post.query.all()

    result = []
    for post in posts:
        result.append(post_to_json(post))

    return jsonify(result)


@app.route("/posts/<id>", methods=["GET"])
def get_post(id):
    post = Post.query.get(id)
    return jsonify(post_to_json(post))


@app.route("/posts", methods=["POST"])
def add_post():
    post = Post(
        id=request.json["id"],
        title=request.json["title"],
        body=request.json["body"],
        user_id=request.json["userId"],
    )

    db.session.add(post)
    db.session.commit()
    return jsonify(request.json)


@app.route("/posts/<id>", methods=["PUT"])
def update_post(id):
    post = Post.query.get(id)
    post.id = request.json["id"]
    post.title = request.json["title"]
    post.body = request.json["body"]
    post.user_id = request.json["userId"]
    db.session.commit()
    return jsonify(post_to_json(post))


@app.route("/posts/<id>", methods=["PATCH"])
def update_post_partial(id):
    post = Post.query.get(id)

    if "id" in request.json:
        post.id = request.json["id"]
    if "title" in request.json:
        post.title = request.json["title"]
    if "body" in request.json:
        post.body = request.json["body"]
    if "userId" in request.json:
        post.user_id = request.json["userId"]

    db.session.commit()
    return jsonify(post_to_json(post))


@app.route("/posts/<id>", methods=["DELETE"])
def delete_post(id):
    Post.query.filter_by(id=id).delete()
    db.session.commit()
    return f"Deleted post with id {id}"


def post_to_json(post):
    return {
        "id": post.id,
        "title": post.title,
        "body": post.body,
        "userId": post.user_id,
    }