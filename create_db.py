from app import db, Post
import requests

db.create_all()

r = requests.get("https://jsonplaceholder.typicode.com/posts")

posts = r.json()

for post in posts:
    new_post = Post(
        id=post["id"], title=post["title"], body=post["body"], user_id=post["userId"]
    )
    db.session.add(new_post)
    db.session.commit()
