# ORMAE-test

### for start server
`
Docker-compose up
`

### baseurl = http://localhost:5000/ 
### Vaild end point
GET	/posts  
GET	/posts/1  
GET	/posts/1/comments  
GET	/comments?postId=1  
GET	/posts?userId=1  
POST	/posts  
PUT	/posts/1  
PATCH	/posts/1  
DELETE	/posts/1  